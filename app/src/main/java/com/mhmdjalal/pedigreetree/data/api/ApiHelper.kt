package com.mhmdjalal.pedigreetree.data.api

import com.mhmdjalal.pedigreetree.data.model.User
import com.mhmdjalal.pedigreetree.data.model.response.CharacterResponse
import retrofit2.Response

/**
 * @author Created by Muhamad Jalaludin on 15/04/2021
 */
interface ApiHelper {

    suspend fun getUsers(): Response<List<User>>

    suspend fun getCharacters(): Response<CharacterResponse>
}