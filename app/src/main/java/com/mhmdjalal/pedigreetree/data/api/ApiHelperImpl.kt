package com.mhmdjalal.pedigreetree.data.api

import com.mhmdjalal.pedigreetree.data.model.User
import com.mhmdjalal.pedigreetree.data.model.response.CharacterResponse
import retrofit2.Response
import javax.inject.Inject

/**
 * @author Created by Muhamad Jalaludin on 15/04/2021
 */
class ApiHelperImpl @Inject constructor(private val apiService: ApiService) : ApiHelper {

    override suspend fun getUsers(): Response<List<User>> = apiService.getUsers()

    override suspend fun getCharacters(): Response<CharacterResponse> = apiService.getCharacters()

}