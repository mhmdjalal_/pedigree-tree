package com.mhmdjalal.pedigreetree.data.api

import com.mhmdjalal.pedigreetree.data.model.User
import com.mhmdjalal.pedigreetree.data.model.response.CharacterResponse
import retrofit2.Response
import retrofit2.http.GET

/**
 * @author Created by Muhamad Jalaludin on 15/04/2021
 */
interface ApiService {

    @GET("users")
    suspend fun getUsers(): Response<List<User>>

    @GET("character")
    suspend fun getCharacters(): Response<CharacterResponse>

}