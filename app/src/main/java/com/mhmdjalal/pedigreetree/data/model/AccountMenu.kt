package com.mhmdjalal.pedigreetree.data.model

/**
 * @author Created by Muhamad Jalaludin on 21/04/2021
 */
data class AccountMenu(
    val icon: Int? = null,
    val title: String? = null,
    val appVersion: String? = null,
    val child: List<AccountMenu> = emptyList()
)