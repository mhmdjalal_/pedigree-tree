package com.mhmdjalal.pedigreetree.data.model

/**
 * @author Created by Muhamad Jalaludin on 21/04/2021
 */
data class FolderTree(
    val id: Int,
    val title: String
)