package com.mhmdjalal.pedigreetree.data.model

/**
 * @author Created by Muhamad Jalaludin on 23/04/2021
 */
data class Inbox(
    val title: String,
    val message: String,
    val date: String,
    var isRead: Boolean = false
)