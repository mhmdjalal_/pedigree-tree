package com.mhmdjalal.pedigreetree.data.model

/**
 * @author Created by Muhamad Jalaludin on 21/04/2021
 */
data class ParentFolderTree(
    val id: Int,
    val title: String? = null,
    val isRecentlyOpen: Boolean = false,
    val child: List<FolderTree> = emptyList()
)