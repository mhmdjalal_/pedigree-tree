package com.mhmdjalal.pedigreetree.data.model.response


import com.mhmdjalal.pedigreetree.data.model.Info
import com.mhmdjalal.pedigreetree.data.model.Character
import com.squareup.moshi.Json
import com.squareup.moshi.JsonClass

@JsonClass(generateAdapter = true)
data class CharacterResponse(
    @Json(name = "info")
    val info: Info,
    @Json(name = "results")
    val results: List<Character>
)