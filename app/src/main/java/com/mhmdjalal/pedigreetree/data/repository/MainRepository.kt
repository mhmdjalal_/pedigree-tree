package com.mhmdjalal.pedigreetree.data.repository

import com.mhmdjalal.pedigreetree.data.api.ApiHelper
import javax.inject.Inject

/**
 * @author Created by Muhamad Jalaludin on 15/04/2021
 */
class MainRepository @Inject constructor(private val apiHelper: ApiHelper) {

    suspend fun getUsers() =  apiHelper.getUsers()

    suspend fun getCharacters() =  apiHelper.getCharacters()

    /*fun getCharacters(results: (Resource<CharacterResponse>) -> Unit) {
        request(scope, { apiHelper.getCharacters() }, {
            results(it)
        })
    }*/
}