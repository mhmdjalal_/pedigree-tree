package com.mhmdjalal.pedigreetree.di

import com.mhmdjalal.pedigreetree.data.api.ApiHelper
import com.mhmdjalal.pedigreetree.data.repository.MainRepository
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.components.ViewModelComponent
import dagger.hilt.android.scopes.ViewModelScoped

/**
 * @author Created by Muhamad Jalaludin on 16/04/2021
 */
@Module
@InstallIn(ViewModelComponent::class)
internal object ViewModelModule {
    @Provides
    @ViewModelScoped
    fun provideMainRepo(apiHelper: ApiHelper) = MainRepository(apiHelper)
}