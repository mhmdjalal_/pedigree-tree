package com.mhmdjalal.pedigreetree.ui.adapter

import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.mhmdjalal.pedigreetree.R
import com.mhmdjalal.pedigreetree.data.model.AccountMenu
import com.mhmdjalal.pedigreetree.databinding.ItemAccountMenuBinding
import com.mhmdjalal.pedigreetree.utils.ComponentViewUtil.gone
import com.mhmdjalal.pedigreetree.utils.ComponentViewUtil.visible
import com.mhmdjalal.pedigreetree.utils.viewBinding

/**
 * @author Created by Muhamad Jalaludin on 15/04/2021
 */
class AccountMenuAdapter(private val listener: (String?) -> Unit) : RecyclerView.Adapter<AccountMenuAdapter.DataViewHolder>() {

    private var menus: List<AccountMenu> = arrayListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) : DataViewHolder {
        val binding = parent.viewBinding(ItemAccountMenuBinding::inflate)
        return DataViewHolder(binding)
    }

    override fun getItemCount(): Int = menus.size

    override fun onBindViewHolder(holder: DataViewHolder, position: Int) {
        holder.bindItem(menus[position])
    }

    inner class DataViewHolder(private val binding: ItemAccountMenuBinding) : RecyclerView.ViewHolder(binding.root) {
        private val context = itemView.context

        fun bindItem(item: AccountMenu) = with(binding) {
            Glide.with(context)
                .load(item.icon)
                .error(android.R.color.darker_gray)
                .into(imageIcon)
            textTitle.text = item.title
            textAppVersion.text = "Version ${item.appVersion}"

            if (item.appVersion.isNullOrEmpty()) {
                textAppVersion.gone()
                container.setCardBackgroundColor(ContextCompat.getColor(context, R.color.white))
                textTitle.setTextColor(ContextCompat.getColor(context, R.color.colorBlack03))
            } else {
                textAppVersion.visible()
                container.setCardBackgroundColor(ContextCompat.getColor(context, R.color.colorRed03))
                textTitle.setTextColor(ContextCompat.getColor(context, R.color.white))
            }

            itemView.setOnClickListener {
                listener(item.title)
            }
        }
    }

    fun setData(list: List<AccountMenu>) {
        menus = list
        notifyDataSetChanged()
    }
}