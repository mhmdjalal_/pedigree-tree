package com.mhmdjalal.pedigreetree.ui.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mhmdjalal.pedigreetree.data.model.AccountMenu
import com.mhmdjalal.pedigreetree.databinding.ItemParentAccountMenuBinding
import com.mhmdjalal.pedigreetree.utils.ComponentViewUtil.gone
import com.mhmdjalal.pedigreetree.utils.ComponentViewUtil.visible
import com.mhmdjalal.pedigreetree.utils.viewBinding

/**
 * @author Created by Muhamad Jalaludin on 15/04/2021
 */
class AccountMenuParentAdapter(private val listener: (String?) -> Unit) : RecyclerView.Adapter<AccountMenuParentAdapter.DataViewHolder>() {

    private var users: List<AccountMenu> = arrayListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) : DataViewHolder {
        val binding = parent.viewBinding(ItemParentAccountMenuBinding::inflate)
        return DataViewHolder(binding)
    }

    override fun getItemCount(): Int = users.size

    override fun onBindViewHolder(holder: DataViewHolder, position: Int) {
        holder.bindItem(users[position])
    }

    inner class DataViewHolder(private val binding: ItemParentAccountMenuBinding) : RecyclerView.ViewHolder(binding.root) {
        private val context = itemView.context

        fun bindItem(item: AccountMenu) = with(binding) {
            textTitle.text = item.title

            if (item.title.isNullOrEmpty()) {
                textTitle.gone()
            } else {
                textTitle.visible()
            }

            val adapter = AccountMenuAdapter {
                listener(it)
            }
            adapter.setData(item.child)
            recyclerMenus.adapter = adapter
            recyclerMenus.layoutManager = LinearLayoutManager(context)
        }
    }

    fun setData(list: List<AccountMenu>) {
        users = list
        notifyDataSetChanged()
    }
}