package com.mhmdjalal.pedigreetree.ui.adapter

import android.view.Gravity
import android.view.MenuInflater
import android.view.ViewGroup
import androidx.appcompat.view.ContextThemeWrapper
import androidx.appcompat.widget.PopupMenu
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.mhmdjalal.pedigreetree.R
import com.mhmdjalal.pedigreetree.data.model.FolderTree
import com.mhmdjalal.pedigreetree.databinding.ItemFolderTreeBinding
import com.mhmdjalal.pedigreetree.utils.viewBinding

/**
 * @author Created by Muhamad Jalaludin on 22/04/2021
 */
class FolderTreeAdapter(
    private val isRecentlyOpen: Boolean,
    private val listener: (String, FolderTree) -> Unit
) : RecyclerView.Adapter<FolderTreeAdapter.DataViewHolder>() {

    private var folderTrees: List<FolderTree> = arrayListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DataViewHolder {
        val binding = parent.viewBinding(ItemFolderTreeBinding::inflate)
        return DataViewHolder(binding)
    }

    override fun getItemCount(): Int = folderTrees.size

    override fun onBindViewHolder(holder: DataViewHolder, position: Int) {
        holder.bindItem(folderTrees[position])
    }

    inner class DataViewHolder(private val binding: ItemFolderTreeBinding) :
        RecyclerView.ViewHolder(binding.root) {
        private val context = itemView.context

        fun bindItem(item: FolderTree) = with(binding) {
            val icon = if (isRecentlyOpen) R.drawable.ic_folder_open else R.drawable.ic_folder
            Glide.with(context)
                .load(icon)
                .error(android.R.color.darker_gray)
                .into(imageIcon)

            textTitle.text = item.title

            itemView.setOnClickListener {
                listener(DETAIL, item)
            }
            btnMore.setOnClickListener {
                val wrapper = ContextThemeWrapper(context, R.style.MyPopupMenu)
                val popup = PopupMenu(wrapper, btnMore, Gravity.END, 0, R.style.MyPopupMenu)
                val inflater: MenuInflater = popup.menuInflater
                inflater.inflate(R.menu.menu_rename, popup.menu)

                popup.setOnMenuItemClickListener { menuItem ->
                    when (menuItem.itemId) {
                        R.id.action_rename -> {
                            listener(RENAME, item)
                            true
                        }
                        else -> false
                    }
                }

                popup.show()
            }
        }
    }

    fun setData(list: List<FolderTree>) {
        folderTrees = list
        notifyDataSetChanged()
    }

    companion object {
        const val RENAME = "rename"
        const val DETAIL = "detail"
    }
}