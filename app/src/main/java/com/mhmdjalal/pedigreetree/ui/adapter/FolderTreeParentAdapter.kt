package com.mhmdjalal.pedigreetree.ui.adapter

import android.graphics.Typeface
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.mhmdjalal.pedigreetree.data.model.FolderTree
import com.mhmdjalal.pedigreetree.data.model.ParentFolderTree
import com.mhmdjalal.pedigreetree.databinding.ItemParentFolderTreeBinding
import com.mhmdjalal.pedigreetree.utils.viewBinding


/**
 * @author Created by Muhamad Jalaludin on 22/04/2021
 */
class FolderTreeParentAdapter(private val listener: (String, FolderTree) -> Unit) :
    RecyclerView.Adapter<FolderTreeParentAdapter.DataViewHolder>() {

    private var folderTrees: List<ParentFolderTree> = arrayListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DataViewHolder {
        val binding = parent.viewBinding(ItemParentFolderTreeBinding::inflate)
        return DataViewHolder(binding)
    }

    override fun getItemCount(): Int = folderTrees.size

    override fun onBindViewHolder(holder: DataViewHolder, position: Int) {
        holder.bindItem(folderTrees[position])
    }

    inner class DataViewHolder(private val binding: ItemParentFolderTreeBinding) :
        RecyclerView.ViewHolder(binding.root) {
        private val context = itemView.context

        fun bindItem(item: ParentFolderTree) = with(binding) {
            val face = if (item.isRecentlyOpen) {
                Typeface.createFromAsset(context.assets, "fonts/roboto_medium_italic.ttf")
            } else {
                Typeface.createFromAsset(context.assets, "fonts/roboto_medium.ttf")
            }
            textTitle.typeface = face
            textTitle.text = item.title

            val adapter = FolderTreeAdapter(item.isRecentlyOpen) { type, folderTree ->
                listener(type, folderTree)
            }
            adapter.setData(item.child)
            recyclerTree.adapter = adapter
            recyclerTree.layoutManager = LinearLayoutManager(context)
        }
    }

    fun setData(list: List<ParentFolderTree>) {
        folderTrees = list
        notifyDataSetChanged()
    }
}