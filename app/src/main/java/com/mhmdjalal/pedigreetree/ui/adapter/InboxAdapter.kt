package com.mhmdjalal.pedigreetree.ui.adapter

import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.mhmdjalal.pedigreetree.R
import com.mhmdjalal.pedigreetree.data.model.Inbox
import com.mhmdjalal.pedigreetree.databinding.ItemInboxBinding
import com.mhmdjalal.pedigreetree.utils.viewBinding

/**
 * @author Created by Muhamad Jalaludin on 22/04/2021
 */
class InboxAdapter(
    private val listener: (Inbox) -> Unit
) : RecyclerView.Adapter<InboxAdapter.DataViewHolder>() {

    private var inboxes: List<Inbox> = arrayListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DataViewHolder {
        val binding = parent.viewBinding(ItemInboxBinding::inflate)
        return DataViewHolder(binding)
    }

    override fun getItemCount(): Int = inboxes.size

    override fun onBindViewHolder(holder: DataViewHolder, position: Int) {
        holder.bindItem(inboxes[position])
    }

    inner class DataViewHolder(private val binding: ItemInboxBinding) :
        RecyclerView.ViewHolder(binding.root) {
        private val context = itemView.context

        fun bindItem(item: Inbox) = with(binding) {
            val backgroundColor = if (item.isRead) {
                ContextCompat.getColor(context, R.color.colorCardHasRead)
            } else {
                ContextCompat.getColor(context, R.color.colorWhite)
            }
            container.setCardBackgroundColor(backgroundColor)

            textTitle.text = item.title
            textMessage.text = item.message
            textDate.text = item.date

            itemView.setOnClickListener {
                listener(item)
            }
        }
    }

    fun setData(list: List<Inbox>) {
        inboxes = list
        notifyDataSetChanged()
    }
}