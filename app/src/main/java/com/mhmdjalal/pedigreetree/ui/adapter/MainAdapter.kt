package com.mhmdjalal.pedigreetree.ui.adapter

import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.mhmdjalal.pedigreetree.data.model.Character
import com.mhmdjalal.pedigreetree.databinding.ItemLayoutBinding
import com.mhmdjalal.pedigreetree.utils.viewBinding

/**
 * @author Created by Muhamad Jalaludin on 15/04/2021
 */
class MainAdapter : RecyclerView.Adapter<MainAdapter.DataViewHolder>() {

    private var users: List<Character> = arrayListOf()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) : DataViewHolder {
        val binding = parent.viewBinding(ItemLayoutBinding::inflate)
        return DataViewHolder(binding)
    }

    override fun getItemCount(): Int = users.size

    override fun onBindViewHolder(holder: DataViewHolder, position: Int) {
        holder.bindItem(users[position])
    }

    inner class DataViewHolder(private val binding: ItemLayoutBinding) : RecyclerView.ViewHolder(binding.root) {
        private val context = itemView.context

        fun bindItem(item: Character) = with(binding) {
            textViewUserName.text = item.name
            textViewUserEmail.text = "${item.species} - ${item.status}"
            Glide.with(context)
                .load(item.image)
                .error(android.R.color.darker_gray)
                .into(imageViewAvatar)
        }
    }

    fun addData(list: List<Character>) {
        users = list
    }
}