package com.mhmdjalal.pedigreetree.ui.view

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.mhmdjalal.pedigreetree.databinding.ActivityAboutBinding
import com.mhmdjalal.pedigreetree.utils.viewBinding

/**
 * @author Created by Muhamad Jalaludin on 22/04/2021
 */
class AboutActivity : AppCompatActivity() {

    private val binding by viewBinding(ActivityAboutBinding::inflate)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        setupToolbar()
    }

    private fun setupToolbar() = with(binding.toolbar) {
        textTitleToolbar.text = "About"
        btnBack.setOnClickListener {
            onBackPressed()
        }
    }
}