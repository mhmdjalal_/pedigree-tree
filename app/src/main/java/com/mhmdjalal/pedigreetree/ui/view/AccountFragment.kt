package com.mhmdjalal.pedigreetree.ui.view

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.mhmdjalal.pedigreetree.R
import com.mhmdjalal.pedigreetree.databinding.DialogConfirmationDefaultBinding
import com.mhmdjalal.pedigreetree.databinding.FragmentAccountBinding
import com.mhmdjalal.pedigreetree.ui.adapter.AccountMenuParentAdapter
import com.mhmdjalal.pedigreetree.ui.viewModel.MainViewModel
import com.mhmdjalal.pedigreetree.utils.viewBinding
import com.mhmdjalal.pedigreetree.utils.viewBindingDialog
import dagger.hilt.android.AndroidEntryPoint
import org.jetbrains.anko.startActivity

/**
 * @author Created by Muhamad Jalaludin on 15/04/2021
 */
@AndroidEntryPoint
class AccountFragment : Fragment(R.layout.fragment_account) {

    private val binding by viewBinding(FragmentAccountBinding::bind)

    private val mainViewModel by activityViewModels<MainViewModel>()
    private lateinit var adapter: AccountMenuParentAdapter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mainViewModel.fetchAccountMenus()
        setupUI()
        setupObserver()

        Glide.with(this)
            .load("https://lh3.googleusercontent.com/-bNZ3L1tXyiY/AAAAAAAAAAI/AAAAAAAAAAA/AMZuuclu0DVSLAiPRq2mSm3Z7XCsBcXX2w/photo.jpg?sz=100")
            .error(android.R.color.darker_gray)
            .into(binding.imageProfile)
    }

    private fun setupUI() = with(binding) {
        adapter = AccountMenuParentAdapter {
            when (it) {
                "Inbox" -> {
                    requireActivity().startActivity<InboxActivity>()
                }
                "Edit Profile" -> {
                    requireActivity().startActivity<EditProfileActivity>()
                }
                "Change Password" -> {
                    requireActivity().startActivity<ChangePasswordActivity>()
                }
                "Your Unique Code" -> {
                    requireActivity().startActivity<UniqueCodeActivity>()
                }
                "About" -> {
                    requireActivity().startActivity<AboutActivity>()
                }
                "Sign Out" -> {
                    signOut()
                }
            }
        }
        recyclerMenus.adapter = adapter
        recyclerMenus.layoutManager = LinearLayoutManager(requireContext())
    }

    private fun setupObserver() {
        mainViewModel.accountMenus.observe(viewLifecycleOwner, {
            adapter.setData(it)
        })

    }

    private fun signOut() {
        val binding = requireContext().viewBindingDialog(DialogConfirmationDefaultBinding::inflate)
        val builder = MaterialAlertDialogBuilder(requireContext())
        builder.apply {
            setView(binding.root)
            setCancelable(false)
        }

        val alert = builder.create()
        with(binding) {
            textTitle.text = "Sign Out"
            textMessage.text = "Are you sure want to sign out ?"
            btnNo.setOnClickListener {
                alert.dismiss()
            }
            btnYes.setOnClickListener {
                requireActivity().startActivity<SignInActivity>()
                requireActivity().finish()
            }
        }

        if (!alert.isShowing) {
            alert.show()
        }
    }
}