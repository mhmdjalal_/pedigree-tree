package com.mhmdjalal.pedigreetree.ui.view

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.mhmdjalal.pedigreetree.databinding.ActivityChangePasswordBinding
import com.mhmdjalal.pedigreetree.utils.viewBinding

/**
 * @author Created by Muhamad Jalaludin on 22/04/2021
 */
class ChangePasswordActivity : AppCompatActivity() {

    private val binding by viewBinding(ActivityChangePasswordBinding::inflate)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        setupToolbar()

    }

    private fun setupToolbar() = with(binding.toolbar) {
        textTitleToolbar.text = "Change Password"
        btnBack.setOnClickListener {
            onBackPressed()
        }
    }
}