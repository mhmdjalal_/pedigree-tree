package com.mhmdjalal.pedigreetree.ui.view

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.bumptech.glide.Glide
import com.mhmdjalal.pedigreetree.databinding.ActivityEditProfileBinding
import com.mhmdjalal.pedigreetree.utils.viewBinding

/**
 * @author Created by Muhamad Jalaludin on 22/04/2021
 */
class EditProfileActivity : AppCompatActivity() {
    private val binding by viewBinding(ActivityEditProfileBinding::inflate)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        setupToolbar()

        Glide.with(this)
            .load("https://lh3.googleusercontent.com/-bNZ3L1tXyiY/AAAAAAAAAAI/AAAAAAAAAAA/AMZuuclu0DVSLAiPRq2mSm3Z7XCsBcXX2w/photo.jpg?sz=100")
            .error(android.R.color.darker_gray)
            .into(binding.imageProfile)
    }

    private fun setupToolbar() = with(binding.toolbar) {
        textTitleToolbar.text = "Edit Profile"
        btnBack.setOnClickListener {
            onBackPressed()
        }
    }
}