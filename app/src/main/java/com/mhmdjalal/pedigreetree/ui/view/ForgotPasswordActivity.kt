package com.mhmdjalal.pedigreetree.ui.view

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.mhmdjalal.pedigreetree.databinding.ActivityForgotPasswordBinding
import com.mhmdjalal.pedigreetree.utils.viewBinding

/**
 * @author Created by Muhamad Jalaludin on 22/04/2021
 */
class ForgotPasswordActivity : AppCompatActivity() {

    private val binding by viewBinding(ActivityForgotPasswordBinding::inflate)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
    }
}