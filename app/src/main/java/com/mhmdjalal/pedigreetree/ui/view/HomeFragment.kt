package com.mhmdjalal.pedigreetree.ui.view

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import com.bumptech.glide.Glide
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.mhmdjalal.pedigreetree.R
import com.mhmdjalal.pedigreetree.data.model.FolderTree
import com.mhmdjalal.pedigreetree.databinding.DialogRenameFolderTreeBinding
import com.mhmdjalal.pedigreetree.databinding.FragmentHomeBinding
import com.mhmdjalal.pedigreetree.ui.adapter.FolderTreeAdapter
import com.mhmdjalal.pedigreetree.ui.adapter.FolderTreeParentAdapter
import com.mhmdjalal.pedigreetree.ui.viewModel.MainViewModel
import com.mhmdjalal.pedigreetree.utils.GreetingUtil
import com.mhmdjalal.pedigreetree.utils.viewBinding
import com.mhmdjalal.pedigreetree.utils.viewBindingDialog
import dagger.hilt.android.AndroidEntryPoint
import org.jetbrains.anko.startActivity

/**
 * @author Created by Muhamad Jalaludin on 15/04/2021
 */
@AndroidEntryPoint
class HomeFragment : Fragment(R.layout.fragment_home) {

    private val binding by viewBinding(FragmentHomeBinding::bind)

    private val mainViewModel by activityViewModels<MainViewModel>()
    private lateinit var adapter: FolderTreeParentAdapter
    private lateinit var greetingUtil: GreetingUtil

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setupUI()
        setupObserver()

        Glide.with(this)
            .load("https://lh3.googleusercontent.com/-bNZ3L1tXyiY/AAAAAAAAAAI/AAAAAAAAAAA/AMZuuclu0DVSLAiPRq2mSm3Z7XCsBcXX2w/photo.jpg?sz=100")
            .error(android.R.color.darker_gray)
            .into(binding.imageProfile)

        mainViewModel.fetchFolderTrees()
    }

    private fun setupUI() = with(binding) {
        adapter = FolderTreeParentAdapter { type, item ->
            if (type == FolderTreeAdapter.RENAME) {
                renameFolderTree(item)
            } else if (type == FolderTreeAdapter.DETAIL) {
                requireContext().startActivity<FamilyTreeDetailActivity>()
            }
        }
        recyclerView.adapter = adapter
        recyclerView.layoutManager = LinearLayoutManager(requireContext())

        greetingUtil = GreetingUtil()
        textGreetings.text = "Hi, ${greetingUtil.getGreetings()}"
    }

    private fun setupObserver() {
        mainViewModel.folderTrees.observe(viewLifecycleOwner, {
            adapter.setData(it)
        })
    }

    private fun renameFolderTree(item: FolderTree) {
        val binding = requireContext().viewBindingDialog(DialogRenameFolderTreeBinding::inflate)
        val builder = MaterialAlertDialogBuilder(requireContext())
        builder.apply {
            setView(binding.root)
            setCancelable(false)
        }

        val alert = builder.create()
        with(binding) {
            editName.setText(item.title)
            btnClose.setOnClickListener {
                alert.dismiss()
            }
            btnSave.setOnClickListener {

            }
        }

        if (!alert.isShowing) {
            alert.show()
        }
    }
}