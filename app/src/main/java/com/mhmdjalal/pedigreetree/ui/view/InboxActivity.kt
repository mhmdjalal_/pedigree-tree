package com.mhmdjalal.pedigreetree.ui.view

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.mhmdjalal.pedigreetree.R
import com.mhmdjalal.pedigreetree.databinding.ActivityInboxBinding
import com.mhmdjalal.pedigreetree.ui.adapter.InboxAdapter
import com.mhmdjalal.pedigreetree.ui.viewModel.MainViewModel
import com.mhmdjalal.pedigreetree.utils.ComponentViewUtil.disabled
import com.mhmdjalal.pedigreetree.utils.ComponentViewUtil.enabled
import com.mhmdjalal.pedigreetree.utils.viewBinding
import dagger.hilt.android.AndroidEntryPoint

/**
 * @author Created by Muhamad Jalaludin on 23/04/2021
 */
@AndroidEntryPoint
class InboxActivity : AppCompatActivity() {

    private val binding by viewBinding(ActivityInboxBinding::inflate)

    private val mainViewModel by viewModels<MainViewModel>()
    private lateinit var adapter: InboxAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        setupToolbar()

        setupUI()
        setupObserver()

        mainViewModel.fetchInboxes()

        binding.btnReadAll.setOnClickListener {
            mainViewModel.fetchInboxesHasRead()
        }
    }

    private fun setupToolbar() = with(binding.toolbar) {
        textTitleToolbar.text = "Inbox"
        btnBack.setOnClickListener {
            onBackPressed()
        }
    }

    private fun setupUI() = with(binding) {
        adapter = InboxAdapter {

        }
        recyclerInbox.adapter = adapter
        recyclerInbox.layoutManager = LinearLayoutManager(this@InboxActivity)
    }

    private fun setupObserver() {
        mainViewModel.inboxes.observe(this, { inboxes ->
            adapter.setData(inboxes)

            val unreadCount = inboxes.filter { !it.isRead }
            val colorButton = if (unreadCount.isNotEmpty()) {
                binding.btnReadAll.enabled()
                R.color.colorButton
            } else {
                binding.btnReadAll.disabled()
                R.color.colorGray03
            }
            binding.btnReadAll.setTextColor(ContextCompat.getColor(this, colorButton))
            binding.btnReadAll.iconTint = ContextCompat.getColorStateList(this, colorButton)
        })
    }
}