package com.mhmdjalal.pedigreetree.ui.view

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.commit
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.mhmdjalal.pedigreetree.R
import com.mhmdjalal.pedigreetree.databinding.ActivityMainBinding
import com.mhmdjalal.pedigreetree.databinding.DialogRenameFolderTreeBinding
import com.mhmdjalal.pedigreetree.utils.viewBinding
import com.mhmdjalal.pedigreetree.utils.viewBindingDialog
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : AppCompatActivity() {

    private val binding by viewBinding(ActivityMainBinding::inflate)

    private var currentTabIndex = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        binding.bottomNavigationView.setOnNavigationItemSelectedListener { menuItem ->
            val fragment: Fragment
            when (menuItem.itemId) {
                R.id.menu_home -> {
                    currentTabIndex = 0
                    fragment = HomeFragment()
                }
                R.id.menu_account ->  {
                    currentTabIndex = 2
                    fragment = AccountFragment()
                }
                else -> {
                    currentTabIndex = 0
                    fragment = HomeFragment()
                }
            }
            supportFragmentManager.commit {
                replace(R.id.content, fragment)
            }
            true
        }
        binding.bottomNavigationView.selectedItemId = R.id.menu_home
        binding.fabAdd.setOnClickListener {
            newFamilyTree()
        }
    }

    private fun newFamilyTree() {
        val binding = viewBindingDialog(DialogRenameFolderTreeBinding::inflate)
        val builder = MaterialAlertDialogBuilder(this)
        builder.apply {
            setView(binding.root)
            setCancelable(false)
        }

        val alert = builder.create()
        with(binding) {
            textTitle.text = "New Family Tree"
            btnClose.setOnClickListener {
                alert.dismiss()
            }
            btnSave.setOnClickListener {

            }
        }

        if (!alert.isShowing) {
            alert.show()
        }
    }
}
