package com.mhmdjalal.pedigreetree.ui.view

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.mhmdjalal.pedigreetree.databinding.ActivitySignInBinding
import com.mhmdjalal.pedigreetree.databinding.DialogForgotPasswordBinding
import com.mhmdjalal.pedigreetree.utils.viewBinding
import com.mhmdjalal.pedigreetree.utils.viewBindingDialog
import org.jetbrains.anko.startActivity

/**
 * @author Created by Muhamad Jalaludin on 15/04/2021
 */
class SignInActivity : AppCompatActivity() {

    private val binding by viewBinding(ActivitySignInBinding::inflate)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        binding.btnSignUp.setOnClickListener {
            startActivity<SignUpActivity>()
            finish()
        }
        binding.btnSignIn.setOnClickListener {
            startActivity<MainActivity>()
            finish()
        }
        binding.btnForgotPassword.setOnClickListener {
            confirmation()
        }
    }

    private fun confirmation() {
        val binding = viewBindingDialog(DialogForgotPasswordBinding::inflate)
        val builder = MaterialAlertDialogBuilder(this)
        builder.apply {
            setView(binding.root)
            setCancelable(false)
        }

        val alert = builder.create()
        with(binding) {
            btnClose.setOnClickListener {
                alert.dismiss()
            }
            btnSend.setOnClickListener {
                startActivity<ForgotPasswordActivity>()
            }
        }

        if (!alert.isShowing) {
            alert.show()
        }
    }
}