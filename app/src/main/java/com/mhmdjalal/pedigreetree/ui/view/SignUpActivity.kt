package com.mhmdjalal.pedigreetree.ui.view

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.mhmdjalal.pedigreetree.databinding.ActivitySignUpBinding
import com.mhmdjalal.pedigreetree.utils.viewBinding
import org.jetbrains.anko.startActivity

/**
 * @author Created by Muhamad Jalaludin on 15/04/2021
 */
class SignUpActivity : AppCompatActivity() {

    private val binding by viewBinding(ActivitySignUpBinding::inflate)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        binding.btnSignIn.setOnClickListener {
            startActivity<SignInActivity>()
            finish()
        }
    }
}