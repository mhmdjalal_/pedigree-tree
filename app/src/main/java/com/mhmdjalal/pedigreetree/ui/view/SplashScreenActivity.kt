package com.mhmdjalal.pedigreetree.ui.view

import android.os.Bundle
import android.os.Handler
import android.os.Looper
import android.view.animation.AnimationUtils
import androidx.appcompat.app.AppCompatActivity
import com.mhmdjalal.pedigreetree.R
import com.mhmdjalal.pedigreetree.databinding.ActivitySplashScreenBinding
import com.mhmdjalal.pedigreetree.utils.viewBinding
import org.jetbrains.anko.startActivity

/**
 * @author Created by Muhamad Jalaludin on 15/04/2021
 */
class SplashScreenActivity : AppCompatActivity() {

    private val binding by viewBinding(ActivitySplashScreenBinding::inflate)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)

        Handler(Looper.getMainLooper()).postDelayed({
            startActivity<SignInActivity>()
            finish()
        }, 1500)

        val myanim = AnimationUtils.loadAnimation(this, R.anim.splashanim)
        binding.container.startAnimation(myanim)
    }
}