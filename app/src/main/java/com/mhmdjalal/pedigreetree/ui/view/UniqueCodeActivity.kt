package com.mhmdjalal.pedigreetree.ui.view

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.mhmdjalal.pedigreetree.databinding.ActivityUniqueCodeBinding
import com.mhmdjalal.pedigreetree.utils.RandomString
import com.mhmdjalal.pedigreetree.utils.viewBinding
import org.jetbrains.anko.toast
import java.security.SecureRandom


/**
 * @author Created by Muhamad Jalaludin on 22/04/2021
 */
class UniqueCodeActivity : AppCompatActivity() {

    private val binding by viewBinding(ActivityUniqueCodeBinding::inflate)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(binding.root)
        setupToolbar()

        val tickets = RandomString(6, SecureRandom())

        binding.textUniqueCode.text = tickets.nextString()
        binding.textUniqueCode.setOnClickListener {
            val clipboard = getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
            val clip = ClipData.newPlainText("unique_code", binding.textUniqueCode.text)
            clipboard.setPrimaryClip(clip)
            toast("Disalin")
        }
        binding.btnRegenerate.setOnClickListener {
            binding.textUniqueCode.text = tickets.nextString()
        }
    }

    private fun setupToolbar() = with(binding.toolbar) {
        textTitleToolbar.text = "Unique Code"
        btnBack.setOnClickListener {
            onBackPressed()
        }
    }
}