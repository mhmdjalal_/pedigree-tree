package com.mhmdjalal.pedigreetree.ui.viewModel

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.mhmdjalal.pedigreetree.R
import com.mhmdjalal.pedigreetree.data.model.AccountMenu
import com.mhmdjalal.pedigreetree.data.model.FolderTree
import com.mhmdjalal.pedigreetree.data.model.Inbox
import com.mhmdjalal.pedigreetree.data.model.ParentFolderTree
import com.mhmdjalal.pedigreetree.data.model.response.CharacterResponse
import com.mhmdjalal.pedigreetree.data.repository.MainRepository
import com.mhmdjalal.pedigreetree.utils.NetworkHelper
import com.mhmdjalal.pedigreetree.utils.Resource
import com.mhmdjalal.pedigreetree.utils.request
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.launch
import javax.inject.Inject

/**
 * @author Created by Muhamad Jalaludin on 15/04/2021
 */
@HiltViewModel
class MainViewModel @Inject constructor(
    private val mainRepository: MainRepository,
    private val networkHelper: NetworkHelper
) : ViewModel() {

    private val _characters = MutableLiveData<Resource<CharacterResponse>>()
    val characters: LiveData<Resource<CharacterResponse>>
        get() = _characters

    private val _accountMenus = MutableLiveData<List<AccountMenu>>()
    val accountMenus: LiveData<List<AccountMenu>>
        get() = _accountMenus

    private val _folderTrees = MutableLiveData<List<ParentFolderTree>>()
    val folderTrees: LiveData<List<ParentFolderTree>>
        get() = _folderTrees

    private val _inboxes = MutableLiveData<List<Inbox>>()
    val inboxes: LiveData<List<Inbox>>
        get() = _inboxes

    init {
        fetchCharacters()
    }

    fun fetchCharacters() {
        Log.e("FETCH", "sure")
        viewModelScope.launch {
            if (networkHelper.isNetworkConnected()) {
                request(viewModelScope, { mainRepository.getCharacters() }, {
                    when (it.status) {
                        Resource.Status.ERROR -> {
                            _characters.postValue(Resource.error(it.message, null))
                        }
                        Resource.Status.LOADING -> {
                            _characters.postValue(Resource.loading(null))
                        }
                        Resource.Status.SUCCESS -> {
                            _characters.postValue(Resource.success(it.data))
                        }
                    }
                })
            } else {
                _characters.postValue(Resource.error("No internet connection", null))
            }
        }
    }

    fun fetchAccountMenus() {
        val arr = arrayListOf<AccountMenu>()

        val arrInbox = arrayListOf<AccountMenu>()
        arrInbox.add(AccountMenu(icon = R.drawable.ic_notifications, "Inbox"))
        arr.add(AccountMenu(child = arrInbox))

        val arrAccount = arrayListOf<AccountMenu>()
        arrAccount.add(AccountMenu(icon = R.drawable.ic_account_circle_18, "Edit Profile"))
        arrAccount.add(AccountMenu(icon = R.drawable.ic_padlock_18, "Change Password"))
        arrAccount.add(AccountMenu(icon = R.drawable.ic_link, "Your Unique Code"))
        arr.add(AccountMenu(title = "Account", child = arrAccount))

        val arrHelp = arrayListOf<AccountMenu>()
        arrHelp.add(AccountMenu(icon = R.drawable.ic_info, "About"))
        arr.add(AccountMenu(title = "Help", child = arrHelp))

        val arrSignOut = arrayListOf<AccountMenu>()
        arrSignOut.add(AccountMenu(icon = R.drawable.ic_log_out, "Sign Out", appVersion = "0.1.0"))
        arr.add(AccountMenu(title = "Sign Out", child = arrSignOut))

        _accountMenus.value = arr
    }

    fun fetchFolderTrees() {
        val arr = arrayListOf<ParentFolderTree>()

        val arrInbox = arrayListOf<FolderTree>()
        arrInbox.add(FolderTree(id = 1, title = "Family Tree 1"))
        arr.add(
            ParentFolderTree(
                id = 1,
                title = "Recently Opened",
                child = arrInbox,
                isRecentlyOpen = true
            )
        )

        val arrTree = arrayListOf<FolderTree>()
        arrTree.add(FolderTree(id = 2, title = "Family Tree 2"))
        arrTree.add(FolderTree(id = 3, title = "Family Tree 3"))
        arr.add(
            ParentFolderTree(
                id = 2,
                title = "Your Family Trees",
                child = arrTree,
                isRecentlyOpen = false
            )
        )

        _folderTrees.value = arr
    }

    fun fetchInboxes() {
        val message =
            "Pedigree tree is a mobile application for creating a family tree. With this application, it is hoped that you can get to know your extended family more closely. Bring and share every precious moment with \"Pedigree Tree\"."
        val arr = arrayListOf<Inbox>()
        arr.add(Inbox(title = "What's new?", message = message, date = "03/04/2021", isRead = true))
        arr.add(Inbox(title = "What's new?", message = message, date = "03/04/2021"))
        arr.add(Inbox(title = "What's new?", message = message, date = "03/04/2021", isRead = true))
        arr.add(Inbox(title = "What's new?", message = message, date = "03/04/2021", isRead = true))
        arr.add(Inbox(title = "What's new?", message = message, date = "03/04/2021"))
        arr.add(Inbox(title = "What's new?", message = message, date = "03/04/2021", isRead = true))
        arr.add(Inbox(title = "What's new?", message = message, date = "03/04/2021"))
        arr.add(Inbox(title = "What's new?", message = message, date = "03/04/2021", isRead = true))
        arr.add(Inbox(title = "What's new?", message = message, date = "03/04/2021"))
        arr.add(Inbox(title = "What's new?", message = message, date = "03/04/2021"))

        _inboxes.value = arr
    }

    fun fetchInboxesHasRead() {
        val message =
            "Pedigree tree is a mobile application for creating a family tree. With this application, it is hoped that you can get to know your extended family more closely. Bring and share every precious moment with \"Pedigree Tree\"."
        val arr = arrayListOf<Inbox>()
        arr.add(Inbox(title = "What's new?", message = message, date = "03/04/2021", isRead = true))
        arr.add(Inbox(title = "What's new?", message = message, date = "03/04/2021", isRead = true))
        arr.add(Inbox(title = "What's new?", message = message, date = "03/04/2021", isRead = true))
        arr.add(Inbox(title = "What's new?", message = message, date = "03/04/2021", isRead = true))
        arr.add(Inbox(title = "What's new?", message = message, date = "03/04/2021", isRead = true))
        arr.add(Inbox(title = "What's new?", message = message, date = "03/04/2021", isRead = true))
        arr.add(Inbox(title = "What's new?", message = message, date = "03/04/2021", isRead = true))
        arr.add(Inbox(title = "What's new?", message = message, date = "03/04/2021", isRead = true))
        arr.add(Inbox(title = "What's new?", message = message, date = "03/04/2021", isRead = true))
        arr.add(Inbox(title = "What's new?", message = message, date = "03/04/2021", isRead = true))

        _inboxes.value = arr
    }
}