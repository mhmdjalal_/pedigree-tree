package com.mhmdjalal.pedigreetree.utils

import android.annotation.SuppressLint
import android.graphics.text.LineBreaker
import android.text.Editable
import android.text.TextWatcher
import android.view.MotionEvent
import android.view.View
import android.widget.EditText
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.textfield.TextInputLayout
import com.mhmdjalal.pedigreetree.utils.StringUtil.addThousandSeparator

/**
 * @author Created by Muhamad Jalaludin on 16/04/2021
 */
object ComponentViewUtil {

    /**
     * This view is visible
     */
    fun View.visible() {
        visibility = View.VISIBLE
    }

    /**
     * This view is invisible, but it still takes up space for layout purposes.
     */
    fun View.invisible() {
        visibility = View.INVISIBLE
    }

    /**
     * This view is invisible, and it doesn't take any space for layout purposes.
     */
    fun View.gone() {
        visibility = View.GONE
    }

    /**
     * This view is enabled
     */
    fun View.enabled() {
        isEnabled = true
    }

    /**
     * This view is disabled
     */
    fun View.disabled() {
        isEnabled = false
    }

    /**
     * View transparency reduced to 0.5 (still clickable)
     */
    fun View.disable() {
        alpha = 0.5f
    }

    /**
     * The transparency of the view is fully displayed
     */
    fun View.enable() {
        alpha = 1f
    }

    /**
     * SwipeRefreshLayout -> Stop refreshing
     */
    fun SwipeRefreshLayout.stopRefreshing() {
        isRefreshing = false
    }

    /**
     * SwipeRefreshLayout -> Start refreshing
     */
    fun SwipeRefreshLayout.startRefreshing() {
        isRefreshing = true
    }

    /**
    * ViewPager2
    * @param sensitivity = semakin kecil nilainya semakin mudah digeser
    */
    fun ViewPager2.reduceDragSensitivity(sensitivity: Int = 5) {
        val recyclerViewField = ViewPager2::class.java.getDeclaredField("mRecyclerView")
        recyclerViewField.isAccessible = true
        val recyclerView = recyclerViewField.get(this) as RecyclerView

        val touchSlopField = RecyclerView::class.java.getDeclaredField("mTouchSlop")
        touchSlopField.isAccessible = true
        val touchSlop = touchSlopField.get(recyclerView) as Int
        touchSlopField.set(recyclerView, touchSlop * sensitivity)       // "8" was obtained experimentally
    }

    /**
     * TextView -> to justify text alignment
     * (only runs on android version O and above)
     */
    fun TextView.justify() {
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.Q) {
            this.justificationMode = LineBreaker.JUSTIFICATION_MODE_INTER_WORD
        }
    }

    /**
     * ImageView -> load image with circular progress
     */
//    fun ImageView.loadImage(context: Context, url: String?) {
//        if (url.isNullOrEmpty()) return
//        val circularProgressDrawable = Utils.circularProgress(context)
//
//        Glide.with(context)
//                .load(url)
//                .apply(RequestOptions()
//                        .diskCacheStrategy(DiskCacheStrategy.ALL)
//                        .placeholder(circularProgressDrawable))
//                .error(R.drawable.ic_user)
//                .into(this)
//    }

    /**
     * ImageView -> load image with circular progress (circle shape)
     */
//    fun CircleImageView.loadImage(context: Context, url: String?) {
//        if (url.isNullOrEmpty()) return
//        val circularProgressDrawable = Utils.circularProgress(context)
//
//        Glide.with(context)
//                .load(url)
//                .apply(RequestOptions()
//                        .diskCacheStrategy(DiskCacheStrategy.ALL)
//                        .placeholder(circularProgressDrawable))
//                .error(R.drawable.ic_user)
//                .into(this)
//    }

    /**
     * EditText -> Enable scrolling when the layout get focus
     */
    @SuppressLint("ClickableViewAccessibility")
    fun View.enableEditTextScroll() {
        this.setOnTouchListener { view, motionEvent ->
            if (this.hasFocus()) {
                view.parent.requestDisallowInterceptTouchEvent(true)
                when (motionEvent.action and MotionEvent.ACTION_MASK) {
                    MotionEvent.ACTION_SCROLL -> {
                        view.parent.requestDisallowInterceptTouchEvent(false)
                        return@setOnTouchListener true
                    }
                }
            }
            return@setOnTouchListener false
        }
    }

    /**
     * add thousand separator to form input/text box
     */
    @SuppressLint("ClickableViewAccessibility")
    fun View.addThousandSeparatorListener() {
        if (this is EditText) {
            this.addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(s: Editable?) {}

                override fun beforeTextChanged(s: CharSequence?, p1: Int, p2: Int, p3: Int) {}

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                    if (s.toString().isNotEmpty()) {
                        this@addThousandSeparatorListener.removeTextChangedListener(this)

                        val cleanString = s.toString().replace(".", "")

                        val nominal = cleanString.toDouble().addThousandSeparator()

                        this@addThousandSeparatorListener.setText(nominal)
                        this@addThousandSeparatorListener.setSelection(nominal.length)

                        this@addThousandSeparatorListener.addTextChangedListener(this)
                    }
                }
            })
        } else if (this is TextInputLayout) {
            this.editText?.addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(s: Editable?) {}

                override fun beforeTextChanged(s: CharSequence?, p1: Int, p2: Int, p3: Int) {}

                override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                    if (s.toString().isNotEmpty()) {
                        this@addThousandSeparatorListener.editText?.removeTextChangedListener(this)

                        val cleanString = s.toString().replace(".", "")

                        val nominal = cleanString.toDouble().addThousandSeparator()

                        this@addThousandSeparatorListener.editText?.setText(nominal)
                        this@addThousandSeparatorListener.editText?.setSelection(nominal.length)

                        this@addThousandSeparatorListener.editText?.addTextChangedListener(this)
                    }
                }
            })
        }
    }
}