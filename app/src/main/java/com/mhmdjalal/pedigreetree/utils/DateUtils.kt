package com.mhmdjalal.pedigreetree.utils

import android.annotation.SuppressLint
import java.text.ParseException
import java.text.SimpleDateFormat
import java.util.*
import java.util.concurrent.TimeUnit

/**
 * @author Created by Muhamad Jalaludin on 16/04/2021
 */
object DateUtils {

    @JvmStatic
    fun fromUnixTime(unixTime: Long): Date {
        return Date(unixTime * 1000L)
    }

    /**
     * parsing date with string format to Date format
     */
    @JvmStatic
    fun fromDateString(dateTimeString: String, format:String = "yyyy-MM-dd"): Date? {
        val simpleDateFormat = SimpleDateFormat(format, Locale.US)
        return try {
            simpleDateFormat.parse(dateTimeString)
        } catch (e: ParseException) {
            null
        }
    }

    /**
     * parsing date with string format to Date format
     */
    @JvmStatic
    fun fromDateChat(dateTimeString: String, format:String = "yyyy-MM-dd HH:mm"): Date? {
        val simpleDateFormat = SimpleDateFormat(format)
        return try {
            simpleDateFormat.parse(dateTimeString)
        } catch (e: ParseException) {
            null
        }

    }

    /**
     * parsing date format to string
     */
    @JvmStatic
    fun toISOString(date: Date): String {
        return SimpleDateFormat("yyyy-MM-dd", Locale.US).format(date)
    }

    /**
     * parsing date format to string
     */
    @JvmStatic
    fun toISODateTimeString(date: Date): String {
        return SimpleDateFormat("yyyy-MM-dd HH:mm:ss", Locale.US).format(date)
    }

    /**
     * get date with pattern 'yyyyMMddHHmmss'
     */
    @JvmStatic
    fun code(date: Date): String {
        return SimpleDateFormat("yyyyMMddHHmmss", Locale.US).format(date)
    }

    /**
     * parsing date format to string
     */
    @JvmStatic
    fun toDisplayString(date: Date?): String? {
        if (date == null) return null
        return SimpleDateFormat("dd MMM yyyy", Locale.US).format(date)
    }

    /**
     * parsing date format to string
     */
    @JvmStatic
    fun toDisplayChat(date: Date?): String? {
        if (date == null) return null
        return SimpleDateFormat("dd MMM yyyy HH:mm", Locale.US).format(date)
    }

    /**
     * calculate the difference in days
     * @return difference of days
     */
    fun getCalculateDay(tglCheck: String?): Int {
        var jmlhari = 1
        val myFormat = SimpleDateFormat("yyyy-MM-dd")
        val today = myFormat.format(Date())

        return try {
            val date1 = myFormat.parse(tglCheck)
            val date2 = myFormat.parse(today)
            val diff = date2.time - date1.time
            jmlhari = TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS).toInt()
            jmlhari + 1
        } catch (e: ParseException) {
            e.printStackTrace()
            jmlhari
        } catch (e: NullPointerException) {
            e.printStackTrace()
            jmlhari
        } catch (e: KotlinNullPointerException) {
            e.printStackTrace()
            jmlhari
        }
    }

    /**
     * get the hours and minutes of the date
     */
    @JvmStatic
    fun toTimeString(date: Date?, locale: Locale = Locale.US): String? {
        if (date == null) return null
        return SimpleDateFormat("HH:mm", locale).format(date)
    }

    /**
     * get data with pattern 'yyyyMMdd' for id of log
     */
    @JvmStatic
    fun idLog(date: Date?): String {
        if (date == null) return "0"
        return SimpleDateFormat("yyyyMMdd", Locale.US).format(date)
    }

    /**
     * get today date
     * @return today date in Date object
     */
    @JvmStatic
    fun getToday(): Date {
        val calendar = Calendar.getInstance()
        calendar.set(Calendar.HOUR_OF_DAY, 0)
        calendar.set(Calendar.MINUTE, 0)
        calendar.set(Calendar.SECOND, 0)
        calendar.set(Calendar.MILLISECOND, 0)

        return calendar.time
    }

    /**
     * parsing date from 'yyyy-MM-dd HH:mm:ss.SSS' format to 'dd MMM yyyy HH:mm'
     */
    @SuppressLint("SimpleDateFormat")
    @JvmStatic
    fun parseDate(time: String): String? {
        val parser = SimpleDateFormat("yyyy-MM-dd HH:mm:ss.SSS")
        val formatter = SimpleDateFormat("dd MMM yyyy HH:mm")

        return formatter.format(parser.parse(time))
    }

    /**
     * parsing date from 'yyyy-MM-dd' format to 'EEEE, d MMMM yyyy'
     * e.g, 2021-02-01 to Monday, 1 February 2021
     * EEEE -> name of the day - e.g., Monday
     * d -> date day - e.g., 1
     * MMMM -> name of the month - e.g., January
     * yyyy -> date year - e.g., 2021
     * @return the parsed date
     */
    @JvmStatic
    fun dateDayWithName(dateTimeString: String?): String {
        if (dateTimeString.isNullOrEmpty()) {
            return ""
        }
        val simpleDateFormat = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
        val date = simpleDateFormat.parse(dateTimeString)

        return SimpleDateFormat("EEEE, d MMMM yyyy", Locale.getDefault()).format(date)
    }

}
