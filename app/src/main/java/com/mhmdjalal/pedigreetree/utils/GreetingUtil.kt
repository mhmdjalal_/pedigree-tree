package com.mhmdjalal.pedigreetree.utils

import java.util.*

/**
 * @author Created by Muhamad Jalaludin on 24/04/2021
 */
class GreetingUtil {

    fun getGreetings(): String {
        val calendar = Calendar.getInstance()
        val hour = calendar.get(Calendar.HOUR_OF_DAY)
        val minutes = calendar.get(Calendar.MINUTE)
        return if (hour >= 5 && (hour < 12 && minutes <= 59)) {
            // good morning
            "Good Morning"
        } else if (hour >= 12 && (hour < 18 && minutes <= 59)) {
            // good afternoon
            "Good Afternoon"
        } else {
            // good evening
            "Good Evening"
        }
    }

}