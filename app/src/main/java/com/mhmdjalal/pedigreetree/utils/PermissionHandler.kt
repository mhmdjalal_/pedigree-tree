package com.mhmdjalal.pedigreetree.utils

import android.Manifest
import android.content.pm.PackageManager
import androidx.activity.ComponentActivity
import androidx.activity.result.ActivityResultLauncher
import androidx.core.app.ActivityCompat

/**
 * @author Created by Muhamad Jalaludin on 16/04/2021
 */
object PermissionHandler {

    /**
     * helper to check the storage permission is granted or not
     * permissions: Camera Access, Read External Storage, Write External Storage
     */
    fun ComponentActivity.checkStoragePermissionsGranted(launcher: ActivityResultLauncher<Array<String>>, response: (Boolean) -> Unit) {
        val isPermissionGranted = ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED

        if (isPermissionGranted) {
            response(true)
        } else {
            val permissions = arrayOf(Manifest.permission.CAMERA, Manifest.permission.READ_EXTERNAL_STORAGE, Manifest.permission.WRITE_EXTERNAL_STORAGE)
            launcher.launch(permissions)
        }
    }

    /**
     * helper to check the location permission is granted or not
     * permissions: ACCESS_FINE_LOCATION, ACCESS_COARSE_LOCATION
     */
    fun ComponentActivity.checkLocationPermissionsGranted(launcher: ActivityResultLauncher<Array<String>>, response: (Boolean) -> Unit) {
        val isPermissionGranted = ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED

        if (isPermissionGranted) {
            response(true)
        } else {
            val permissions = arrayOf(Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.ACCESS_COARSE_LOCATION)
            launcher.launch(permissions)
        }
    }
}