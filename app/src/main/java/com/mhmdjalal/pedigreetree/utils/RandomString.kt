package com.mhmdjalal.pedigreetree.utils

import java.util.*

/**
 * @author Created by Muhamad Jalaludin on 22/04/2021
 */
class RandomString(length: Int, random: Random) {
    /**
     * Generate a random string.
     */
    fun nextString(): String {
        for (idx in buf.indices) buf[idx] = symbols[random.nextInt(symbols.size)]
        return String(buf)
    }

    private val random: Random
    private val symbols: CharArray
    private val buf: CharArray

    companion object {
        private const val upper = "ABCDEFGHIJKLMNOPQRSTUVWXYZ"
        private val lower = upper.toLowerCase(Locale.ROOT)
        private const val digits = "0123456789"
    }

    init {
        require(length >= 1)
        this.random = Objects.requireNonNull(random)
        this.symbols = (upper + lower + digits).toCharArray()
        buf = CharArray(length)
    }
}