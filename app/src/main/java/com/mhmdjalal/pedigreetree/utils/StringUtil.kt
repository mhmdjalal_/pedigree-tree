package com.mhmdjalal.pedigreetree.utils

import java.text.DecimalFormat
import java.text.NumberFormat

/**
 * @author Created by Muhamad Jalaludin on 16/04/2021
 */
object StringUtil {

    private const val thousandSeparator = '.'
    private const val decimalSeparator = ','

    /**
     * helper to convert phone number with separator '-'
     * e.g., 081290807754 to 0812-9080-7754
     */
    fun String?.convertPhoneNumberWithSeparator(): String {
        if (this.isNullOrEmpty()) return "-"

        val phoneNew = "0${this.substring(2, this.length)}"
        val phoneFirst = phoneNew.substring(0, 4)
        val phoneSecond = phoneNew.substring(4, 8)
        val phoneThird = phoneNew.substring(8)

        return "$phoneFirst-$phoneSecond-$phoneThird"
    }

    /**
     * helper to change the prefix of phone number
     * from 62 to 0
     */
    fun String?.convertPhoneNumber(): String {
        if (this.isNullOrEmpty()) return ""

        return if (this.startsWith("62")) {
            "0${this.substring(2, this.length)}"
        } else {
            this
        }
    }

    /**
     * helper to change some characters of phone number
     * that will be replaced with '*'
     */
    fun String?.convertSecretPhoneNumber(): String {
        if (this.isNullOrEmpty()) return "-"

        val phoneNumber = if (this.startsWith("62")) {
            "0${this.substring(2, this.length)}"
        } else {
            this
        }

        val prefix = phoneNumber.substring(0, 3)
        val suffix = phoneNumber.substring(phoneNumber.length - 3, phoneNumber.length)
        val secret = phoneNumber.substring(3, phoneNumber.length - 3)
        var secretNumber = ""
        for (i in 1..secret.length) {
            secretNumber += "*"
        }

        return "$prefix$secretNumber$suffix"
    }

    /**
     * helper to change some characters of email
     * that will be replaced with '*'
     */
    fun String?.convertSecretEmail(): String {
        if (this.isNullOrEmpty()) return "-"

        val emailName = this.substringBefore("@")
        val prefix = if (emailName.length > 5) this.substring(0, 2) else this.substring(0, 1)
        val suffix = emailName.substring(emailName.length - 1, emailName.length) + "@" + this.substringAfter("@")

        val startIndex = if (emailName.length > 5) 2 else 1
        val secret = emailName.substring(startIndex, emailName.length - 1)
        var secretEmail = ""
        for (i in 1..secret.length) {
            secretEmail += "*"
        }

        return "$prefix$secretEmail$suffix"
    }

    /**
     * helper to change the Int to currency format
     * e.g., 123456 to Rp 123.456
     */
    fun Int?.currencyFormatter(): String {
        val formatter = NumberFormat.getInstance() as DecimalFormat
        val symbols = formatter.decimalFormatSymbols
        symbols.groupingSeparator = thousandSeparator
        formatter.decimalFormatSymbols = symbols
        return "Rp ${formatter.format(this ?: 0)}"
    }

    /**
     * helper to change the Int without separator to separator
     * e.g., 123456 to 123.456
     */
    fun Int?.addThousandSeparator(): String {
        val formatter = NumberFormat.getInstance() as DecimalFormat
        val symbols = formatter.decimalFormatSymbols
        symbols.groupingSeparator = thousandSeparator
        formatter.decimalFormatSymbols = symbols
        return formatter.format(this ?: 0)
    }

    /**
     * helper to change the Int without separator to separator
     * e.g., 123456 to 123,456
     */
    fun Int?.addDecimalSeparator(): String {
        val formatter = NumberFormat.getInstance() as DecimalFormat
        val symbols = formatter.decimalFormatSymbols
        symbols.groupingSeparator = decimalSeparator
        formatter.decimalFormatSymbols = symbols
        return formatter.format(this ?: 0)
    }

    /**
     * helper to change the Long to currency format
     * e.g., 123456 to Rp 123.456
     */
    fun Long?.currencyFormatter(): String {
        val formatter = NumberFormat.getInstance() as DecimalFormat
        val symbols = formatter.decimalFormatSymbols
        symbols.groupingSeparator = thousandSeparator
        formatter.decimalFormatSymbols = symbols
        return "Rp ${formatter.format(this ?: 0)}"
    }

    /**
     * helper to change the Long without separator to separator
     * e.g., 123456 to 123.456
     */
    fun Long?.addThousandSeparator(): String {
        val formatter = NumberFormat.getInstance() as DecimalFormat
        val symbols = formatter.decimalFormatSymbols
        symbols.groupingSeparator = thousandSeparator
        formatter.decimalFormatSymbols = symbols
        return formatter.format(this ?: 0)
    }

    /**
     * helper to change the Long without separator to separator
     * e.g., 123456 to 123,456
     */
    fun Long?.addDecimalSeparator(): String {
        val formatter = NumberFormat.getInstance() as DecimalFormat
        val symbols = formatter.decimalFormatSymbols
        symbols.groupingSeparator = decimalSeparator
        formatter.decimalFormatSymbols = symbols
        return formatter.format(this ?: 0)
    }

    /**
     * helper to change the Double to currency format
     * e.g., 123456 to Rp 123.456
     */
    fun Double?.currencyFormatter(): String {
        val formatter = NumberFormat.getInstance() as DecimalFormat
        val symbols = formatter.decimalFormatSymbols
        symbols.groupingSeparator = thousandSeparator
        formatter.decimalFormatSymbols = symbols
        return "Rp ${formatter.format(this ?: 0)}"
    }

    /**
     * helper to change the Double without separator to separator
     * e.g., 123456 to 123.456
     */
    fun Double?.addThousandSeparator(): String {
        val formatter = NumberFormat.getInstance() as DecimalFormat
        val symbols = formatter.decimalFormatSymbols
        symbols.groupingSeparator = thousandSeparator
        formatter.decimalFormatSymbols = symbols
        return formatter.format(this ?: 0)
    }

    /**
     * helper to change the Double without separator to separator
     * e.g., 123456 to 123,456
     */
    fun Double?.addDecimalSeparator(): String {
        val formatter = NumberFormat.getInstance() as DecimalFormat
        val symbols = formatter.decimalFormatSymbols
        symbols.groupingSeparator = decimalSeparator
        formatter.decimalFormatSymbols = symbols
        return formatter.format(this ?: 0)
    }

    /**
     * helper to change the Float to currency format
     * e.g., 123456 to Rp 123.456
     */
    fun Float?.currencyFormatter(): String {
        val formatter = NumberFormat.getInstance() as DecimalFormat
        val symbols = formatter.decimalFormatSymbols
        symbols.groupingSeparator = thousandSeparator
        formatter.decimalFormatSymbols = symbols
        return "Rp ${formatter.format(this ?: 0)}"
    }

    /**
     * helper to change the Float without separator to separator
     * e.g., 123456 to 123.456
     */
    fun Float?.addThousandSeparator(): String {
        val formatter = NumberFormat.getInstance() as DecimalFormat
        val symbols = formatter.decimalFormatSymbols
        symbols.groupingSeparator = thousandSeparator
        formatter.decimalFormatSymbols = symbols
        return formatter.format(this ?: 0)
    }

    /**
     * helper to change the Float without separator to separator
     * e.g., 123456 to 123,456
     */
    fun Float?.addDecimalSeparator(): String {
        val formatter = NumberFormat.getInstance() as DecimalFormat
        val symbols = formatter.decimalFormatSymbols
        symbols.groupingSeparator = decimalSeparator
        formatter.decimalFormatSymbols = symbols
        return formatter.format(this ?: 0)
    }

    /**
     * helper to change the prefix of phone number
     * e.g., from +6281290807754 to 6281290807754
     * e.g., from 081290807754 to 6281290807754
     */
    fun String?.parsePhoneString(): String {
        var phone = this
        if (phone.isNullOrEmpty()) return ""

        if (phone.startsWith("+"))
            phone = phone.replace("+", "")

        if (phone.startsWith("0"))
            phone = "62${phone.substring(1)}"

        return phone
    }

    fun String?.removeLastChar(): String {
        if (this.isNullOrEmpty()) return ""

        return this.substring(0, this.length - 1)
    }
}