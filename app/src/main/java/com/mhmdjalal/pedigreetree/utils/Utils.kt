package com.mhmdjalal.pedigreetree.utils

import android.content.Context
import android.util.TypedValue
import android.view.Gravity
import android.view.View
import android.widget.TextView
import android.widget.Toast
import androidx.annotation.IdRes
import androidx.constraintlayout.widget.ConstraintLayout
import java.io.File
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.util.*

/**
 * @author Created by Muhamad Jalaludin on 16/04/2021
 */
object Utils {

    /**
     * circular progress for load image
     */
//    fun circularProgress(context: Context, @ColorRes color: Int = R.color.colorPrimary): CircularProgressDrawable {
//        val circularProgressDrawable = CircularProgressDrawable(context)
//        circularProgressDrawable.setColorSchemeColors(ContextCompat.getColor(context, color))
//        circularProgressDrawable.strokeWidth = 5f
//        circularProgressDrawable.centerRadius = 30f
//        circularProgressDrawable.start()
//        return circularProgressDrawable
//    }
//
//    fun requestOptionsCircular(context: Context, @ColorRes color: Int = R.color.colorPrimary): RequestOptions {
//        val circularProgress = circularProgress(context, color)
//        return RequestOptions()
//                .diskCacheStrategy(DiskCacheStrategy.ALL)
//                .placeholder(circularProgress)
//    }

    /**
     * remove duplicates data from list
     */
    fun <T> removeDuplicates(list: List<T>): ArrayList<T> {

        // Create a new ArrayList
        val newList = ArrayList<T>()

        // Traverse through the first list
        for (element in list) {

            // If this element is not present in newList
            // then add it
            if (!newList.contains(element)) {
                newList.add(element)
            }
        }

        // return the new list
        return newList
    }

    fun <T> T?.isNull(): Boolean {
        return this == null
    }

    fun <T> T?.isNotNull(): Boolean {
        return this != null
    }

    fun Context.getDimension(value: Float): Int {
        return TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP, value, this.resources.displayMetrics).toInt()
    }

    fun Context.toastCenterMessage(message: String) {
        val toast = Toast.makeText(this, message, Toast.LENGTH_SHORT)
        val v = toast.view?.findViewById<TextView>(android.R.id.message)
        v?.gravity = Gravity.CENTER
        toast.show()
    }

    fun setStatusBarHeight(context: Context, view: View, @IdRes statusBarId: Int) {
        val statusBar = view.findViewById<View>(statusBarId)
        val params = statusBar.layoutParams as ConstraintLayout.LayoutParams
        params.height = getStatusBarHeight(context)
        statusBar.layoutParams = params
    }

    private fun getStatusBarHeight(context: Context): Int {
        var result = 0
        val resourceId = context.resources.getIdentifier("status_bar_height", "dimen", "android")
        if (resourceId > 0) {
            result = context.resources.getDimensionPixelSize(resourceId)
        }
        return result
    }

    fun addRupiahSeparator(value: Int): String {
        val kursIndonesia = DecimalFormat.getCurrencyInstance() as DecimalFormat
        val formatRp = DecimalFormatSymbols.getInstance(Locale.getDefault())

        formatRp.currencySymbol = ""
        formatRp.monetaryDecimalSeparator = ','
        formatRp.groupingSeparator = '.'

        kursIndonesia.decimalFormatSymbols = formatRp
        return kursIndonesia.format(value)
    }

    fun dpToPixels(context: Context, dp: Int): Int {
        val scale = context.resources.displayMetrics.density
        return (dp * scale + 0.5f).toInt()
    }

    fun deleteCache(context: Context) {
        try {
            val dir: File = context.cacheDir
            deleteDir(dir)
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun deleteDir(dir: File?): Boolean {
        return if (dir != null && dir.isDirectory) {
            val children: Array<String> = dir.list()
            for (i in children.indices) {
                val success = deleteDir(File(dir, children[i]))
                if (!success) {
                    return false
                }
            }
            dir.delete()
        } else if (dir != null && dir.isFile) {
            dir.delete()
        } else {
            false
        }
    }
}

